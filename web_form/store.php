<?php
echo '<pre>';
var_dump($_FILES );
echo '<pre>';


$_POST['user_name'];
$_POST['email'];
$_POST['pass'];
$_POST['select'];
$_POST['textarea'];
$_POST['skill'];

$tmp_name = $_FILES['image']['tmp_name'];
$img_name = $_FILES['image']['name'];
move_uploaded_file($tmp_name,'photo/'.$img_name);


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
      <div class="row">
          <div class="col-md-3"></div>
              <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr>
                            <td>Name</td>
                            <td><?php echo $_POST['user_name'] ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?php echo $_POST['email'] ?></td>
                        </tr>

                        <tr>
                            <td>Password</td>
                            <td><?php echo $_POST['pass'] ?></td>
                        </tr>

                        <tr>
                            <td>Batch</td>
                            <td><?php echo $_POST['select'] ?></td>
                        </tr>

                        <tr>
                            <td>Comments</td>
                            <td><?php echo $_POST['textarea'] ?></td>
                        </tr>

                        <tr>
                            <td>Skill</td>
                            <td><?php echo implode(',',$_POST['skill']) ?></td>
                        </tr>
                        <tr>
                            <td>Photo</td>
                            <td><img src="photo/<?php echo $img_name ?>" width="150"></td>
                        </tr>
                    </table>
              </div>
      </div>
  </div>




    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>