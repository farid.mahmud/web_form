
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-3"></div>
                  <div class="col-md-6">
                      <form action="store.php" method="POST" enctype="multipart/form-data">

                              <legend>Student information Form</legend>
                          <div class="form-group">
                              <label for="exampleInputEmail1">User name </label>
                              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter name" name="user_name">
                          </div>

                          <div class="form-group">
                              <label for="exampleInputEmail1">Email address</label>
                              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                          </div>

                          <div class="form-group">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
                          </div>

                          <div class="form-group">
                              <label for="exampleSelect1">Batch</label>
                              <select class="form-control" id="exampleSelect1" name="select">
                                  <option>01</option>
                                  <option>02</option>
                                  <option>03</option>
                                  <option>04</option>
                                  <option>05</option>
                              </select>
                          </div>

                          <div class="form-group">
                              <label for="exampleTextarea">Comments</label>
                              <textarea class="form-control" id="exampleTextarea" rows="3" name="textarea"></textarea>
                          </div>

                          <div class="form-group">
                          <label class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" name="skill[]" value="PHP">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description">PHP</span>
                          </label>

                          <label class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" name="skill[]" value="HTML">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description">HTML</span>
                          </label>
                          </div>

                          <div class="form-group">
                              <label class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" name="skill[]" value="CSS">
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description">CSS</span>
                              </label>

                          <label class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" name="skill[]" value="BOOTSTRAP">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description">BOOTSTRAP</span>
                          </label>
                          </div>

                          <div class="form-group">
                              <label class="custom-file">
                                  <input type="file" id="file" class="custom-file-input" name="image">
                                  <span class="custom-file-control"></span>
                              </label>
                          </div>

                          <button type="submit" class="btn btn-primary">Submit</button>

                      </form>

                  </div>
              </div>
          </div>
    </div>
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>